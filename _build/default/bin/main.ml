let search arr =
    fun param ->
        let tmp = Str.regexp_string param in
        List.filter (fun c -> try ignore (Str.search_forward tmp c 0); true with Not_found -> false) (Array.to_list arr);;

Sys.chdir(Sys.getenv("HOME"));

Printf.printf "Starting in dir: %s\n" (Sys.getcwd());


let arr = Sys.readdir(".") in
let param = Sys.argv.(1) in
let res = (search arr param) in
List.iter (Printf.printf "%s\n") res;;
