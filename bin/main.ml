(*let list_to_array lis = Array.of_list (List.map Array.of_list lis);; *)

let rec search arr =
        fun param ->
            let tmp = Str.regexp_string param in
            let filtered = List.filter
                (fun c -> try ignore (Str.search_forward tmp c 0);
                    true with Not_found -> false)
                (Array.to_list arr) in
            if List.length filtered <> 0 then
                let t = Array.of_list filtered in
                search t param;;

Sys.chdir(Sys.getenv("HOME"));

Printf.printf "Starting in dir: %s\n" (Sys.getcwd());


let arr = Sys.readdir(".") in
let param = Sys.argv.(1) in
let res = (search arr param) in
Array.iter (Printf.printf "%s\n") res;;
